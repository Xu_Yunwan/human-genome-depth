#!/data/home/xuyw/miniconda2/bin/python
import sys,os
in_f=sys.argv[1]
binsize=int(sys.argv[2])
#binsize=1000000
depth_bin=[]

chr_list=[      "chr1","chr2","chr3","chr4","chr5","chr6",
                        "chr7","chr8","chr9","chr10","chr11","chr12",
                        "chr13","chr14","chr15","chr16","chr17","chr18",
                        "chr19","chr20","chr21","chr22","chrX","chrY"
]

fo=open(os.path.splitext(in_f)[0]+".bin%s"%binsize+".1"+os.path.splitext(in_f)[1],'a+')
fo.truncate()
chrm,pos,depth,unknown=range(4)
with open(in_f) as f:
        ini_ln=0
        ln=0
        for line in f:
                line=line.strip()
                ln+=1
                tmp=line.split("\t")
                if ln%binsize==1:
                        current_chr=tmp[chrm]

                if tmp[chrm]==current_chr:
                        try:
                                depth_bin+=[int(tmp[depth])]
                        except:
                                print "[ERROR] | "+in_f+" | "+line+" | ",
                                print tmp
                                pass
                else:
                        ln=ini_ln+1
                        depth_bin=[]

                if ln%binsize==0:#last pos in bin
                        aver_depth=sum(depth_bin)/float(binsize)
                        print >> fo,"%s\t%s\t%s"%(tmp[chrm],tmp[pos],aver_depth)
                        depth_bin=[]
fo.close()
