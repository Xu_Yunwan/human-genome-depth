#!/data/home/xuyw/miniconda2/bin/python
import sys,os
import matplotlib.pyplot as plt
import pandas as pd
plt.switch_backend('agg')
tsv_f=sys.argv[1]
binsize=int(sys.argv[2])
'''
chr_nd_pos=[248,491,
689,879,1061,
1232,1391,
1536,1674,
1808,1943,
2077,2191,
2298,2400,
2490,2574,
2654,2713,
2777,2824,
2875,3031,
3088]
chr_tag_pos=[]
for i,n in enumerate(chr_end_pos):
        if i==0:
                chr_tag_pos.append(n/2)
        else:
                chr_tag_pos.append(n-(chr_end_pos[i]-chr_end_pos[i-1])/2)
        chr_tag_pos.append(n)
chr_list=[]

for x in range(1,23) :
        chr_list.append("chr%s"%x)
        chr_list.append("__")
chr_list.append("chrX")
chr_list.append("__")
chr_list.append("chrY")
#chr_list.append("__")
print chr_list

'''
tb=pd.read_table(tsv_f)
ticksize=20
tb[tb.depth<10]["depth"].plot(kind='bar',figsize=(20,8),fontsize=ticksize,width=0.99)
#plt.plot(tb.index,tb.depth)

plt.xticks([1000,2000,3000],["","",""])
plt.xlabel("(ordered by genomic region)",horizontalalignment="center",fontsize=ticksize)
plt.title("Depth (binsize: %s bp)"%binsize,fontsize=30)
plt.subplots_adjust(left=0.03, right=0.98, top=0.9, bottom=0.1)
plt.savefig(os.path.splitext(tsv_f)[0].replace(".","_"))