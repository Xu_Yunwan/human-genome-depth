#!/bin/sh
set -e
date
lane=$1
binsize=1000000
for x in `seq 1 22` X Y
do
{
        samtools view -h lane_${lane}_bwa_mapping_hg38_sorted.bam chr$x | samtools view -bS -o  lane_${lane}_bwa_mapping_hg38_sorted.chr$x.bam
        samtools depth -a lane_${lane}_bwa_mapping_hg38_sorted.chr$x.bam > lane_${lane}_bwa_mapping_hg38_sorted.chr$x.depth
        norm.depth.py lane_${lane}_bwa_mapping_hg38_sorted.chr${x}.depth $binsize
}&
done
wait

#if [-e "lane_${lane}_bwa_mapping_hg38_sorted.bin${binsize}.depth"];then
#       rm lane_${lane}_bwa_mapping_hg38_sorted.bin${binsize}.depth
#fi
echo -e "chr\tpos\tdepth" > lane_${lane}_bwa_mapping_hg38_sorted.bin${binsize}.depth
for x in `seq 1 22` X Y
do
        cat lane_${lane}_bwa_mapping_hg38_sorted.chr$x.bin${binsize}.1.depth >> lane_${lane}_bwa_mapping_hg38_sorted.bin${binsize}.depth
done

depth.pic.py lane_${lane}_bwa_mapping_hg38_sorted.bin${binsize}.depth $binsize
date
set +e
