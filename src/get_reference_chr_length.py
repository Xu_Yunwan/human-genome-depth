#!/data/home/xuyw/miniconda2/bin/python

#example: @SQ     SN:chrUn_KI270745v1     LN:41891
#Usage: samtools view -H <in.bam> | get_reference_chr_length.py

import sys
f=sys.stdin
for line in f:
        tmp=line.strip().split("\t")
        if tmp[0]=="@SQ":
                chrm=tmp[1].split(":")[1]
                length=tmp[2].split(":")[1]
                f=open(chrm+".length","w")
                f.write(length)
                f.close()
